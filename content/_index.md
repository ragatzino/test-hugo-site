---
title: Contexte et Plan de la formation
type: docs
---

# Formation état de l'art

L'objectif de cette formation est l'acculturation a différents enjeux du développement informatique et leur application à l'intérieur et à l'extérieur de l'INSEE. 

> Les différents sujets abordés sont ceux ci dessous 

{{< columns >}}
## Environnements linux

Les postes de travail sont sur windows mais les environnements cible sont pour la plupart linux (Debian / Ubuntu). Petit tour d'horizon sur le travail sur environnement Linux.

<--->

## Intégration continue

Présentation du concept. Différences phases d'une chaine de build. Mise en place d'intégration continue dans un projet sur Gitlab. 
{{< /columns >}}


{{< columns >}}
## Opensource et configuration

Comment rendre son projet portable et configurable? Exemple avec un projet construit avec Spring Boot. Installation d'une base de données PostgreSQL configurée a partir d'un contrat opensource.

<--->

## Infrastructure as code

Présentation du concept général. Présentation de l'architecture puppet a l'INSEE. Mise en pratique dans la configuration d'un contrat puppet. 
{{< /columns >}}


{{< columns >}}
## Conteneurisation et virtualisation

Présentation des enjeux de la conteneurisation des applications. Mise en place d'un pipeline pour construire un package docker.
<--->

## Intégration continue

Ea _furtique_ risere fratres edidit terrae magis. Colla tam mihi tenebat:
miseram excita suadent es pecudes iam. Concilio _quam_ velatus posset ait quod
nunc! Fragosis suae dextra geruntur functus vulgata.
{{< /columns >}}
